import React from 'react'

const Button = (props) => {
  return(
  <button onClick={props.handleClick.bind(this)}  className="button">
    { props.children }
  </button>
  )
}

export default Button
