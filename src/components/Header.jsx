import React from 'react'
import Search from './../containers/Search'
import logo from './../assets/logo-header-desktop-light.png'

const Header = () => {
  return(
  <header className="header">
    <div className="container">
      <img src={logo} className="logo" alt="logo" />
      <Search/>
    </div>
  </header>
  )
}

export default Header
