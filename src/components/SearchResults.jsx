import React, { Component } from 'react'
import { connect } from 'react-redux'

import Result from './Result'

class SearchResults extends Component {

  createList = () => {
    const data = this.props.searchReducer.results
    let list = []
    for (let i = 0; i < data.length; i++) {
      list.push(
        <Result data={data[i]} key={i}/>
      )
    }
    return list
  }

  render() {
    return (
      <div>
        <br/>
        <h2>Cocktails</h2>
        <div className="searchResults">
          {this.props.searchReducer.results ? this.createList() : 'No cocktails found'}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  ...state
 })

export default connect(mapStateToProps)(SearchResults)
