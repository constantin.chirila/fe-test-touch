import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class Result extends Component {
  goToDetails(){
    this.props.history.push('/cocktail/' + this.props.data.idDrink)
  }
  render() {
    return (
      <div className="result"  onClick={this.goToDetails.bind(this)}>
        <img src={this.props.data.strDrinkThumb} alt=""/>
        <div className="content">
          <h2>{this.props.data.strDrink}</h2>
        </div>

      </div>
    )
  }
}

export default withRouter(Result)
