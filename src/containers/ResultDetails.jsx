import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Button from './../components/Button'
import { detailAction } from './../actions/action'

class ResultDetails extends Component {
  constructor(props){
    super(props)
    this.state = {
      search: this.props.searchReducer.searchTerm ? 'Go back to results' : 'Go back home'
    }
  }
  componentDidMount() {
    this.props.detailAction(this.props.match.params.id)
  }

  goBack(){
    if(this.props.searchReducer.searchTerm) {
      this.props.history.push('/search/' + this.props.searchReducer.searchTerm)
    } else {
      this.props.history.push('/')
    }
  }

  getIngredients() {
    const ingredients = []
    for(let i=0; i <= 14; i++){
      const ingredient = this.props.detailReducer.result['strIngredient' + i]
      const measure = this.props.detailReducer.result['strMeasure' + i]
      if(ingredient){
        ingredients.push(
          <div key={i}>
            <b>{ingredient}</b>: {measure}
          </div>
        )
      }
    }
    return ingredients
  }

  render() {
    if(this.props.detailReducer.result){
      const details = this.props.detailReducer.result
      return (
        <div className="searchResultDetails clear">
          <div className="clear">
          <Button handleClick={this.goBack.bind(this)}>{this.state.search}</Button>
          </div>
          <div className="gallery">
            <img src={details.strDrinkThumb} alt={details.strDrink}/>
          </div>
          <div className="details">
            <h1>{details.strDrink}</h1>
            <p>{details.strInstructions}</p>
            <h5>Ingredients</h5>
            {this.getIngredients()}
          </div>
        </div>
      )
    }
    return (<div></div>)
  }
}

const mapStateToProps = state => ({
  ...state
})

const mapDispatchToProps = dispatch => ({
  detailAction: (id) => dispatch(detailAction(id))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResultDetails))