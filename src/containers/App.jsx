import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import Header from './../components/Header'
import SearchResults from './../components/SearchResults'
import ResultDetails from './ResultDetails'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <main>
            <Route exact path="/search/:id" component={SearchResults} />
            <Route exact path="/cocktail/:id" component={ResultDetails} />
        </main>
      </div>
    );
  }
}


export default App