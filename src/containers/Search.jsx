import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Button from './../components/Button'
import { searchAction } from './../actions/action'

class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchValue: '',
      errorMsg: ''
    }
  }

  searchAction() {
    if(this.state.searchValue) {
      this.props.searchAction(this.state.searchValue)
      this.props.history.push('/search/' + this.state.searchValue)
    } else {
      this.setState({errorMsg: 'Search field is empty. Enter a cocktail name.'})
    }
  }

  handleChange(e) {
    this.setState({errorMsg: ''})
    this.setState({ searchValue: e.target.value })
  }

  handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.searchAction()
    }
  }

  render() {
    return (
      <div className="search">
        <h1>Cocktail Bar Search</h1>
        <div className="searchContainer">
          <input
            type="text"
            placeholder="Cocktail name"
            value={this.state.searchValue}
            onChange={ this.handleChange.bind(this) }
            onKeyPress={this.handleKeyPress.bind(this) }
            />
          <div className="errorMsg">
            {this.state.errorMsg}
          </div>
        </div>
        <Button handleClick={this.searchAction.bind(this)}>Search Cocktail</Button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state
 })

const mapDispatchToProps = dispatch => ({
searchAction: (searchTerm) => dispatch(searchAction(searchTerm))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Search))