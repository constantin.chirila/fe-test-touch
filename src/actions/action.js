import axios from "axios"

export const searchAction = (searchTerm) => dispatch => {
  axios.get('https://www.thecocktaildb.com/api/json/v1/1/search.php?s=' + searchTerm, {}).then(response => {
    dispatch({
      type: 'SEARCH_ACTION',
      payload: {
        data: response.data.drinks,
        searchTerm
      }
     })
  }).catch(err => {
    console.log(err)
  })
 }

 export const detailAction = (id) => dispatch => {
  axios.get('https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=' +id , {}).then(response => {
    dispatch({
      type: 'DETAIL_ACTION',
      payload: response.data
     })
  }).catch(err => {
    console.log(err)
  })
 }