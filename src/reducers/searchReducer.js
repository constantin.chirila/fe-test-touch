export default (state = {}, action) => {
  switch (action.type) {
    case 'SEARCH_ACTION':
    return {
      results: action.payload.data,
      searchTerm: action.payload.searchTerm
    }
   default:
    return state
  }
 }